#include "gavin-clusterable-classes.h"

namespace kaldi {

KMeansClusterable::KMeansClusterable() {}

KMeansClusterable::KMeansClusterable(int32 dim, BaseFloat):
		dim_(dim) {}

KMeansClusterable::KMeansClusterable(const Vector<BaseFloat> &point, const Vector<BaseFloat> &, BaseFloat, BaseFloat count):
        count_(1), dim_(point.Dim()) {

	KALDI_ASSERT(count == 1);
	points_.insert(std::make_shared<Vector<BaseFloat>>(point));
}

KMeansClusterable::KMeansClusterable(const std::set<std::shared_ptr<Vector<BaseFloat>>, VectorComparator> &points, int count, int dim):
        count_(count), dim_(dim), points_(points) {

    KALDI_ASSERT(count == points.size());
}

void KMeansClusterable::SetZero() {
	count_ = 0;
	points_ = std::set<std::shared_ptr<Vector<BaseFloat>>, VectorComparator> ();
	refresh_ = true;
}

void KMeansClusterable::Add(const Clusterable &other_in) {
	KALDI_ASSERT(other_in.Type() == "kmeans");
	const KMeansClusterable &other = static_cast<const KMeansClusterable&>(other_in);

	for(auto point : other.points_) {
		auto ret = points_.insert(point);
		if(ret.second == false)
			KALDI_ERR << "Attempt to Add() a duplicate element";
	}

	count_ += other.count_;
	refresh_ = true;
}

void KMeansClusterable::Sub(const Clusterable &other_in) {
	KALDI_ASSERT(other_in.Type() == "kmeans");
	const KMeansClusterable &other = static_cast<const KMeansClusterable &>(other_in);

	for(auto point : other.points_) {
		auto x = points_.find(point);
		if(x != points_.end())
			points_.erase(x);
		else
			KALDI_ERR << "Attempt to Sub() an element not present in cluster";
	}

	count_ -= other.count_;
	KALDI_ASSERT(count_ == points_.size());
	refresh_ = true;
}

Clusterable* KMeansClusterable::Copy() const {
	return new KMeansClusterable(points_, count_, dim_);
}

BaseFloat KMeansClusterable::Objf() const {
	// if (count_ == 0)
	// 	return 0;

	// Vector<BaseFloat> centroid = x_stats();
	// BaseFloat res = 0;

	// for(auto point : points_) {
	// 	Vector<BaseFloat> temp (point); // copy point
	// 	temp.AddVec(-1, centroid); // coordinate-wise distance from centroid
	// 	res += temp.Norm(2);
	// }

	// return -res / count_; // average distance from centroid

	BaseFloat variance = x2_stats().Sum();
	return -variance * count_;
}

void KMeansClusterable::Write(std::ostream &os, bool binary) const {
	WriteToken(os, binary, "GCL");  // magic string.
	WriteBasicType(os, binary, count_);
	WriteBasicType(os, binary, dim_);

	for(auto point : points_)
		point->Write(os, binary);
}

Clusterable* KMeansClusterable::ReadNew(std::istream &is, bool binary) const {
	KMeansClusterable *kmc = new KMeansClusterable();
	kmc->Read(is, binary);
	return kmc;
}

void KMeansClusterable::Read(std::istream &is, bool binary) {
	SetZero();
	ExpectToken(is, binary, "GCL");
	ReadBasicType(is, binary, &count_);
	ReadBasicType(is, binary, &dim_);

	for(int i = 0; i < count_; i++) {
		Vector<BaseFloat> v (dim_);
		v.Read(is, binary);
		points_.insert(std::make_shared<Vector<BaseFloat>>(v));
	}
	
	refresh_ = true;
}

void KMeansClusterable::AddStats(const VectorBase<BaseFloat> &vec, BaseFloat weight) {
	// KALDI_ERR << "This Clusterable object does not implement AddStats().";
	KALDI_ASSERT(weight == 1);
	const Vector<BaseFloat> v (vec);
	KMeansClusterable kmc (v, v, 0, 1);
	Add(kmc);
	refresh_ = true;
}

Vector<BaseFloat> KMeansClusterable::x_stats() const {
	Vector<BaseFloat> centroid (dim_);

	for(auto point : points_)
		centroid.AddVec(1. / count_, *point);

	return centroid;
}

Vector<BaseFloat> KMeansClusterable::x2_stats() const {
	Vector<BaseFloat> centroid = x_stats();
	Vector<BaseFloat> x2 (dim_);

	for(auto point : points_) {
		Vector<BaseFloat> temp (*point); // copy point
		temp.AddVec(-1, centroid); // coordinate-wise distance from centroid
		x2.AddVec2(1. / count_, temp); // x2 holds average of squared coordinate-wise distances
	}

	return x2;
}

// fast_x_stats() is preferred over x_stats(), but cannot be used internally for const-correctness
Vector<BaseFloat> KMeansClusterable::fast_x_stats() {
	if(refresh_) {
		refresh_ = false;
		centroid_ = x_stats();
	}

	return centroid_;
}

} //namespace
