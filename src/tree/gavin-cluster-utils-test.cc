#include "tree/gavin-clusterable-classes.h"
#include "tree/clusterable-classes.h"

namespace kaldi {

// basic testing of KMeansClusterable class
static void TestKMeansClusterable() {
	int num = 5, dim = 1;
	std::vector<GaussClusterable *> v (num);
	Vector<BaseFloat> emptyVec;

	for(int i = 1; i <= num; i++) {
		Vector<BaseFloat> *vec = new Vector<BaseFloat> (dim);

		for(int j = 0; j < dim; j++)
			(*vec)(j) = i * (j + 1);

		v[i-1] = new GaussClusterable(*vec, emptyVec, 0, 1);
	}

	GaussClusterable cluster (dim, 0);

	for(int i = 0; i < num; i++) {
		cluster.Add(*(v[i]));
		printf("%f %f\n", cluster.x_stats()(0), cluster.Objf());
	}

	// cluster.Add(*v[num-1]);
	// cluster.Sub(*v[num-1]);

	printf("%f %f\n", cluster.x_stats()(0), cluster.Objf());
}

static void TestRand() {
	for(int i = 0; i < 10; i++)
		printf("%d\n", Rand());
}

} //namespace

int main() {
	using namespace kaldi;

	TestKMeansClusterable();
	TestRand();
}
