#ifndef GAVIN_TREE_CLUSTERABLE_CLASSES_H
#define GAVIN_TREE_CLUSTERABLE_CLASSES_H

#include "itf/clusterable-itf.h"
#include "matrix/matrix-lib.h"
#include <set>
#include <memory>

namespace kaldi {

class VectorComparator {
public:
    bool operator() (const std::shared_ptr<Vector<BaseFloat>> &a, const std::shared_ptr<Vector<BaseFloat>> &b) const {
        KALDI_ASSERT(a->Dim() == b->Dim());

        for(int i = 0; i < a->Dim(); i++) {
            if((*a)(i) < (*b)(i))
                return true;
            else if((*a)(i) > (*b)(i))
                return false;
        }

        return false;
    }
};


class KMeansClusterable: public Clusterable {
public:
    KMeansClusterable();
    KMeansClusterable(int32, BaseFloat);
    KMeansClusterable(const Vector<BaseFloat> &, const Vector<BaseFloat> &, BaseFloat, BaseFloat);
    KMeansClusterable(const std::set<std::shared_ptr<Vector<BaseFloat>>, VectorComparator> &, int, int);

    virtual std::string Type() const { return "kmeans"; }
    virtual BaseFloat Objf() const;
    virtual void SetZero();
    virtual void Add(const Clusterable &);
    virtual void Sub(const Clusterable &);
    virtual Clusterable* Copy() const;
    virtual BaseFloat Normalizer() const { return count_; }
    virtual void Write(std::ostream &, bool) const;
    virtual Clusterable* ReadNew(std::istream &, bool) const;

    void AddStats(const VectorBase<BaseFloat> &, BaseFloat = 1);
    void Read(std::istream &, bool);
    BaseFloat count() const { return count_; }
    Vector<BaseFloat> x_stats() const;
    Vector<BaseFloat> x2_stats() const;
    Vector<BaseFloat> fast_x_stats();
    int dim() const { return dim_; }

protected:
    int count_ = 0, dim_ = 0;
    std::set<std::shared_ptr<Vector<BaseFloat>>, VectorComparator> points_;

    bool refresh_ = true;
    Vector<BaseFloat> centroid_;
};

} // namespace

#endif
