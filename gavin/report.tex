% Template for ICASSP-2016 paper; to be used with:
%          spconf.sty  - ICASSP/ICIP LaTeX style file, and
%          IEEEbib.bst - IEEE bibliography style file.
% --------------------------------------------------------------------------
\documentclass{article}
\usepackage{spconf,amsmath,graphicx,amsfonts,framed}

% Example definitions.
% --------------------
\def\x{{\mathbf x}}
\def\L{{\cal L}}

% Title.
% ------
\title{Improving Clustering in Kaldi}
%
% Single address.
% ---------------
\name{Gavin V. Saldanha, gvs2106}%\thanks{Thanks to XYZ agency for funding.}}
\address{Columbia University}
%
% For example:
% ------------
%\address{School\\
%	Department\\
%	Address}
%
% Two addresses (uncomment and modify for two-address case).
% ----------------------------------------------------------
%\twoauthors
%  {A. Author-one, B. Author-two\sthanks{Thanks to XYZ agency for funding.}}
%	{School A-B\\
%	Department A-B\\
%	Address A-B}
%  {C. Author-three, D. Author-four\sthanks{The fourth author performed the work
%	while at ...}}
%	{School C-D\\
%	Department C-D\\
%	Address C-D}
%
\begin{document}
%\ninept
%
\maketitle
%
\begin{abstract}
\vspace{.1cm}
Clustering is an unsupervised learning task where unlabeled data is separated into clusters that maximize the similarity of the points within clusters and minimize the similarity of points in different clusters. It has been studied extensively and there are many popular algorithms for clustering, each with their own merits and drawbacks. Clustering is a critical component of the Kaldi speech recognition system, used both in the monophone stage to build phones by clustering features and in triphone training to build a decision tree from the triphones.\\

We implemented a couple alternatives to the Gaussian Expectation Maximization clustering that is default in Kaldi and tested them against a reduced Voxforge dataset. Specifically, we wrote $k$-Means (using Lloyd's algorithm) and $k$-Harmonic Means using the algorithm described by Zhang et al \cite{1}. By improving this early step in the Kaldi system, we attempt to show how it can have magnified performance rammifications on the accuracy of the system. Although ultimately we are unable to prove this, our algorithm does provide some valuable information and important benefits over the base Expectation Maximization one.\\
\end{abstract}

\section{Introduction}
\label{sec:intro}
Kaldi ships with a variation of Gaussian Expectation Maximization (EM) clustering implemented. Unfortunately, EM tends to run slowly and performs worse than some of the other clustering algorithms. Therefore, we implemented several alternative clustering methods in an attempt to improve overall performance of the Kaldi system.\\

Our main focus was on using $k$-Means and its variants. The $k$-Means clustering algorithm (Lloyd's algorithm) is a well known, simple, and relatively efficient algorithm that divides the data into exactly $k$ distinct clusters. $k$-Means begins by randomly assigning $k$ points as the means of the clusters\footnote{There are several methods of initialization. We will go into this in more depth later}. The algorithm then alternates between the following two steps until no assignments change (or some specified number of iterations has elapsed). In the first step, each data point is assigned to the cluster whose mean is nearest (as defined by some specified metric, usually Euclidean distance). In the second, the means are recomputed as the centroid of the points in the cluster.\\

$k$-Means is generally much faster than EM clustering and has similar performance. However, both algorithms also suffer from sensitivity to their initialization - they both tend to converge to local optima, and thus must be run several times to find a good clustering.\\

The $k$-Harmonic Means algorithm, proposed by Zhang et al., is a slight variation on $k$-Means, changing the way the means are computed from the points. Instead of using the centroid, the means are defined using the harmonic mean of the distances from points to clusters. This method has been shown to drastically reduce the dependence of $k$-Means on its inialization and thus converge more consistently to the global optima. It has also been experimentally shown to provide superior clusterings to both EM and naive $k$-Means.\\

\section{Literature Review}
\label{sec:format}
In \cite{1}, Zhang et al. propose $k$-Harmonic Means. This algorithm follows the same basic structure as naive $k$-Means, but the means $m_k$ are recomputed according to the following:
$$m_k=\left.\sum_i\frac1{d_{i,k}^4\left(\displaystyle\sum_j\frac1{d_{i,j}^2}\right)^2}x_i\middle/\sum_i\frac1{d_{i,k}^4\left(\displaystyle\sum_j\frac1{d_{i,j}^2}\right)^2}\right.$$
where $d_{i,j}$ denotes the distance (according to some predefined distance metric) from point $x_i$ to cluster center $m_j$.\\

As we can see, unlike standard $k$-Means, where each point only contributes to the computation of its own mean, every point contributes to the calculation of every center. The points are given higher weight if they are closer to their cluster center, and the value of the weights is given by the harmonic mean of their distance from the other centers.\\

The harmonic mean is the repricrocal of the average of the reciprocals of each input. In other words, the harmonic mean $H$ of a set $S$ is given by:
$$H=\frac{|S|}{\displaystyle\sum_{x\in S}\frac 1x}$$
In this application, we see the harmonic mean of the squared distances $d_{i,j}$ used to weight the data points.\\

As explained in the paper, this method of computing the centroids is superior. The naive method is highly dependent on the initial choice of centroids. Using the harmonic mean fixes that problem, as the harmonic mean is much more sensitive to points that are close to the centroid. In other words, $k$-harmonic means attempts to create clusters where the distance between the centroid and the closest points is minimized. This makes it significantly less sensitive to outliers - and the farther the outlier, the less impact it has.\\

In \cite{2}, Zhang makes a minor improvement upon his $k$-Harmonic Means algorithm, by using the $L^p$ norm for $p>2$ instead of the $L^2$ norm or Euclidean distance. He shows that this variation provides superior clusterings\footnote{The equation shown above comes from this paper, as the original has some mathematical inconsistencies}.\\

One large challenge in any $k$-Means type algorithm is choosing an appropriate $k$. A brute force scheme might be to start with $k=1$ and increase $k$ incrementally until the total error of the clustering is below some threshold, or $k$ hits some ceiling value. In \cite{3}, Hamerly and Elkan propose a Gaussian-based approach to finding $k$. The key idea is that the data points belonging to a particular cluster should follow a Gaussian distribution. Under this assumption, they develop a G-Means algorithm which begins by clustering using a small value of $k$, and splits a cluster into two if its points are not similar enough to a Gaussian distribution.\\

\cite{4} is an evaluation of the above algorithms and provides the basis for our expectations of this project. Hamerly and Elkan show experimentally how both $k$-Harmonic Means and a fuzzy membership variation of $k$-Means perform significantly better than the naive version on their datasets. They also compare Gaussian Expectation Maximization. They found that both $k$-Means and EM perform very poorly in comparison to the others, with $k$-Means slightly outperforming EM after a few iterations. $k$-Harmonic Means performed the best, especially if an $L^p$ norm with $p>3$ is used as the distance metric.\\

\section{Initial Approach}
\label{sec:pagestyle}
\subsection{Implementation Method}
The first step in implementing this change was to create the KMeansClusterable subclass. This class implements the same methods as the GaussianClusterable class, which is used in the base Kaldi distribution, but adds the necessary support for $k$-Means. Specifically, unlike GaussianClusterable, a KMeansClusterable needed a container holding all the data points belonging to that cluster in order to correctly compute the mean and variance.\\

The Voxforge dataset used to test this implementation has about 250,000 feature vectors. This means that the KMeansClusterable objects can have up to 250,000 elements in them. Thus it was imperative that the container used to store these points was optimized for its use cases. Ultimately, an ordered set was chosen as it provides $O(\log n)$ insertion and deletion and allows for fast iteration. A second critical consideration was memory management. The set could not contain pointers to the data points, as it was not clear when the data points would be deallocated. Initially, we used copies of the points so each KMeansClusterable could deallocate its set when it was deallocated. However, given the large size of the data this caused the program to exceed the RAM and the process would be terminated by the OS with no warning or error message. \verb|std::shared_ptr| ended up giving the final solution; thus when the last Clusterable containing each point is deallocated, then and only then is the point itself deallocated.\\

Next, the actual $k$-Means routine had to be written in \verb|tree/cluster-utils.cc|. This is, unsurprisingly, completely different from the existing EM algorithm. It has 2 stages - initialization and iteration. During the initialization phase, $k$ means are created using one of two methods, random partitioning or the Forgy method. We will discuss these approaches shortly. The iteration phase, in traditional $k$-Means fashion, alternates between reassigning every point to the nearest mean's cluster and updating the means from the data. $k$-Harmonic Means only differs from $k$-Means in its method of updating the means, thus making it easy to adapt the code to either algorithm.\\

There are two basic methods of initialization in $k$-Means and its variants. The first is random partitioning, in which each data point is randomly assigned to a cluster. The initial cluster means are then calculated as the centroid of these clusters. This method of initialization tends to have all the cluster means near the middle of the data, towards the centroid of all the data points. As Hamerly and Elkan show, this method is best used for $k$-Harmonic Means \cite{4}. The second approach is known as the Forgy method and, contrastingly, tends to create initial centers that are spread out. In Forgy initialization, $k$ of the data points are randomly chosen to be the $k$ starting centers. This method is shown to be more accurate for standard $k$-Means and EM clustering \cite{4}. Kaldi's EM clustering, however, does random partitioning, so we edited it to be able to do either using a boolean flag.\footnote{The base implementation also contains a bug that makes the initialization deterministic for $k=2$ instead of being random. We fixed this as well.}

\subsection{Difficulties}
One major limitation of this implementation was its speed. The default Gaussian EM algorithm clusters the data in under a second, while both our $k$-Means and $k$-Harmonic Means took a few minutes. This by itself isn't a huge deal as Kaldi takes several hours to run on Voxforge. However, building the decision tree - the step immediately after clustering - took about an hour when our clustering algorithms are used, instead of a couple minutes using EM. It is not clear why this happened, as Kaldi does not seem to be stuck in loops anywhere. The most likely hypothesis we came up with, is that the deallocation of the clusters as they were combined adds considerable overhead, since the reference counts for all the points in the clusters are updated.\\

This essentially made running the entire Voxforge script impractical, as it would nearly double the runtime of it (to over 10 hours). Thus we needed to find a better way of modeling the clusters.\\

\section{Second Approach}
\label{sec::pagestyle}
\subsection{Implementation Method}
Upon further reflection, it was not actually necessary to keep a set containing the points in each cluster. Clusters needed to be able to compute their size, their centroid, and their variance. In our initial approach, we were computing the centroid and variance in a batch manner, but they can both be computed in an online fashion.\\

The centroid $C$ of a cluster $X$ is simply the componentwise mean of the points:
$$C=\frac1{|X|}\sum_{p\in X}p=\mathbb E(X)$$
The variance of cluster $X$ is given by:
\begin{align*}
\text{Var}(X)&=\mathbb E(X^2)-[\mathbb E(X)]^2\\
&=\mathbb E(X^2)-C^2
\end{align*}
where
$$\mathbb E(X^2)=\frac1{|X|}\sum_{p\in X}p^2$$
where $p^2$ denotes the componentwise squaring of point $p$.\\

Both $\displaystyle\sum_{p\in X}p$ and $\displaystyle\sum_{p\in X}p^2$ can easily be computed as running sums, as each data point is added. Moreover, when two clusters are combined, their respective running sums can be added together to give the correpsonding sum for the supercluster. The size $|X|$ is trivial to compute and track.\\

The builtin GaussClusterable class already does all of this - it keeps a count of the number of elements in the cluster, and running sums of elements and squares of elements. The only thing needed to adapt it to $k$-Means clustering was to add a method for computing the centroid by dividing the sum of elements by their count.\\

Since both GaussClusterable and KMeansClusterable implemented the same methods, replacing KMeansClusterable with GaussClusterable in the actual clustering routine was the only change needed there. Suddenly, without the overhead of sets with several thousand elements, the clustering and tree-building ran almost instantly and provided the same results (the actual clustering performed is identical).\\

\section{Results}
\label{sec:typestyle}
All of our tests were run on the reduced Voxforge dataset, using the \verb|run.sh| script in \verb|egs/voxforge/s5|. The tests consisted of runs of $k$-Means and $k$-Harmonic Means on the data using each initialization method described above. The average Word Error Rate (WER) and Sentence Error Rate (SER) for each of the 4 runs plus the original EM is shown in Figure 1. The $x$-axis is the method used to generate the output - monophone training, initial triphone training using delta features, triphone training using LDA, SAT, MLLT, etc. Due to time constraints (each run would take 5+ hours to complete), we were only able to generate output for the first 15 methods.\\

As can be seen from the results, all the clustering algorithms except for $k$-Means using the random partition initialization strategy perform (approximately) equally well. None of our algorithms were able to beat the Gaussian EM algorithm by any significant margin. This is most likely due to the fact that $k$ is really small. In the Voxforge example scripts, $k=2$ because only binary clustering is done to produce a binary decision tree. With such a small number of clusters, it is unlikely that different algorithms will classify many points differently - at most a couple points will swap. However, $k$-Means with random partitioning has a significantly higher error rate, at both the word and sentence level. This is to be expected, as random partitioning places the cluster centroids near the middle of the data instead of spreading it out.

\begin{figure*}
\begin{framed}
\begin{center}
\centerline{\includegraphics[width=17cm]{result1}}
\vspace{1cm}
\centerline{\includegraphics[width=17cm]{result2}}
\vspace{1cm}
\centerline{Figure 1. Results}
\vspace{-.5cm}
\end{center}
\end{framed}
\end{figure*}

\section{Conclusions}
\label{sec:majhead}
Although there were no consistent or significant improvements to the accuracy of the system, there are still considerable advantages of using our $k$-Means or $k$-Harmonic means code. Most importantly, it is very simple - initialization followed by a single loop. This is in direct contrast to the default EM implementation, which is incredibly difficult to follow, harder to understand, and nearly impossible to debug. Our algorithm also runs slightly faster than the EM one, though this difference is negligible since both run on the order of seconds (and the overall system takes hours).\\

One major conclusion that jumps out is how poorly $k$-Means with random partitioning performed in comparison to the others. As noted in \cite{4}, $k$-Means should be initialized using the Forgy method, and our results corroborated theirs. They also say that $k$-Harmonic Means should use random partitioning, but as $k$-Harmonic Means is much less sensitive to the initial mean placement, it doesn't make a significant difference in our tests. It is very slightly noticeable, especially in the sentence error rate that the one with random partitioning (orange) is slightly below or equal to the Forgy (yellow) line.\\

\section{Limitations and Future Work}
\label{sec:print}
As mentioned earlier, the Voxforge example scripts which were used to test our implementation do not use the full power of Kaldi's clustering. The phones are built from the features without doing clustering - if the features were clustered to form the phones instead, there could be a large improvement. This clustering step would not have $k$ fixed at 2 and thus would leverage the power of a superior algorithm much more.\\

This situation would also lend itself to using the G-Means algorithm from \cite{3}. Because $k$ was restricted to 2 in our tests, it did not make sense to try and apply G-Means. However, we would be interested in exploring whether this method of learning $k$ can provide substantial benefits when $k$ is less restricted.\\

In order to really determine whether $k$-Means or $k$-Harmonic Means makes substantial performance increases over EM, it would be necessary to cluster using a larger $k$. For $k=2$ and only 40 points (as Voxforge has), there simply isn't enough variability to realize a superior algorithm - any reasonable algorithm should perform about as well as any other. A suitable followup would be to run Kaldi on the full Voxforge dataset or another larger set and see if any performance improvements are obtained.\\

Additionally, due to the long running time of the script (~5-6 hours per run), we were unable to run all the tests we would have liked all the way through. However, we did run each of them through several clustering and training passes to base our results and analysis off. In the future, we would like to continue to run these, as well as run some combination scripts - for instance, running $k$-Means or $k$-Harmonic Means to convergence and then applying one or two iterations of EM. Although we did experiment with trying to do this, the current Kaldi codebase for EM is not conducive to applying to existing clusters; it is, as aforementioned, incredibly difficult to modify without affecting the code in unexpected ways due to how convoluted their implementation is.\\

Another limiting factor of $k$-Means is its reliance upon the $L^2$ norm for metric spaces where that is not necessarily the most intuitive measure. Future work might choose a different $L^p$ norm, especially with $p>3$, which Zhang shows to cluster better \cite{2}. This alternate distance metric can be applied to both $k$- and $k$-Harmonic Means easily.

\vfill\pagebreak

%\section{REFERENCES}
%\label{sec:refs}
%\bibliographystyle{IEEEbib}
%\bibliography{bib}

\begin{thebibliography}{1}
\bibitem{1}
Zhang, Bin, Hsu, Meichun and Dayal, Umeshwar. ``K-Harmonic Means - A Data Clustering Algorithm." 1999.

\bibitem{2}
Zhang, Bin. ``Generalized K-Harmonic Means -- Dynamic Weighting of Data in Unsupervised Learning." 2000.

\bibitem{3}
Hamerly, Greg and Elkan, Charles. ``Learning the $k$ in $k$-means." In Neural Information Processing Systems, 2003. MIT Press.

\bibitem{4}
Hamerly, Greg and Elkan, Charles. ``Alternatives to the k-means algorithm that find better clusterings." Proceedings of the Eleventh International Conference on
Information and Knowledge Management, 2002. Pages 600-607.
\end{thebibliography}

\end{document}
